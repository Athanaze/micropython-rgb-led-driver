# Micropython RGB LED driver

Tiny driver to control an RGB LED with PWM.

# Usage

## 1. Put the driver file on the board

`sudo ampy --port /dev/ttyUSB0 put RGBPWM.py`

## 2. Import and create the object.

`from RGBPWM import RGBPWM`

`rgb_pwm = RGBPWM(5, 21, 19) # red, green and blue pins number`

`rgb_pwm.set_colors_from_hex("#00eeff")`
