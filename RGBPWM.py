import math
from machine import Pin, PWM

class RGBPWM():
    '''
    Common              Common
    Anode               Cathode
    ====                ====
    ||||                ||||
    ||||                ||||
    R||B                R||B
     |G                  |G
     +                   -
    
    '''
    def __init__(self, r_pin, g_pin, b_pin):
        self.r = PWM(Pin(r_pin, Pin.OUT), freq=50, duty=0)
        self.g = PWM(Pin(g_pin, Pin.OUT), freq=50, duty=0)
        self.b = PWM(Pin(b_pin, Pin.OUT), freq=50, duty=0)
        
    def set_values(self, ar, ag, ab):
        self.r.duty(ar)
        self.g.duty(ag)
        self.b.duty(ab)
    
    def set_red(self):
        self.set_values(0, 1023, 1023)
    
    def set_green(self):
        self.set_values(1023, 0, 1023)
    
    def set_blue(self):
        self.set_values(1023, 1023, 0)

    
    def set_colors_from_hex(self, color):
        if color[0] == "#":
            color = color[1:]
        
        r_channel = self.convert(color[1] + color[2])
        g_channel = self.convert(color[2] + color[3])
        b_channel = self.convert(color[4] + color[5])

        self.set_values(r_channel, g_channel, b_channel)

    def convert(self, str):
        return abs((int(str, 16) * 4)-1020)